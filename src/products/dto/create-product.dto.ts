import { IsNotEmpty, MinLength, Min } from 'class-validator';
export class CreateProductDto {
  @IsNotEmpty()
  @Min(1)
  id: number;

  @MinLength(8)
  name: string;

  @Min(2)
  price: number;
}
